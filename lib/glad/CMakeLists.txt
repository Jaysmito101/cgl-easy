include_directories(
	./include
	)


set(SOURCES
	./src/glad.c
	)

add_library(glad ${SOURCES})