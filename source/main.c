#include "cgl_easy.h"

CGL_int counter = 0;
CGL_float total_time = 0.0f;

void setup()
{
    CGL_info("Setup");
    counter = 0;
    total_time = 0.0f;
}

void update(CGL_float delta_time)
{
    total_time += delta_time; // add delta time to total timer
    if (total_time >= 1.0f) // this code will run every second
    {
        total_time = 0.0f;
        counter++;
        CGL_info("Counter: %d", counter);
    }

    // reset counter if left mouse button is pressed
    if(CGL_EASY_IS_MOUSE_BUTTON_PRESS(CGL_MOUSE_BUTTON_LEFT)) 
    {
        counter = 0;
        total_time = 0.0f;
    }
}

void draw(CGL_float delta_time)
{
    CGL_widgets_begin();
    char buffer[32];
    sprintf(buffer, "%d", counter);
    CGL_widgets_add_string(buffer, -0.1f, -0.1f, 0.2f, 0.2f);
    CGL_widgets_end();
}

void cleanup()
{
    CGL_info("Cleanup");
}

CGL_EASY_DEFINE_MAIN(setup, update, draw, cleanup);