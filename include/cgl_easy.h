#define CGL_LOGGING_ENABLED
#define CGL_IMPLEMENTATION
#define CGL_EXCLUDE_NETWORKING
#define CGL_EXCLUDE_TEXT_RENDER
#include "cgl.h"


//void update(CGL_float delta_time);
// void draw(CGL_float delta_time);
// void setup();
// void cleanup();

static struct 
{ 
    CGL_window* window; 
    CGL_framebuffer* framebuffer; 
    CGL_bool is_running; 
    CGL_bool is_drawing; 
    CGL_bool is_updating;
} g_context;

#define CGL_EASY_WINDOW g_context.window
#define CGL_EASY_FRAMEBUFFER g_context.framebuffer
#define CGL_EASY_IS_KEY_PRESS(key) (CGL_window_is_key_pressed(g_context.window, key) == CGL_PRESS)
#define CGL_EASY_IS_MOUSE_BUTTON_PRESS(key) (CGL_window_get_mouse_button(g_context.window, key) == CGL_PRESS)
#define CGL_EASY_DEFINE_MAIN(setup, update, draw, cleanup) \
static void CGL_easy_close() { g_context.is_running = false; } \
static void CGL_easy_no_update() { g_context.is_updating = false; } \
static void CGL_easy_no_draw() { g_context.is_drawing = false; } \
static void CGL_easy_update() { g_context.is_updating = true; } \
static void CGL_easy_draw() { g_context.is_drawing = true; } \
int main()\
{\
    srand((uint32_t)time(NULL));\
    if(!CGL_init()) return EXIT_FAILURE;\
    g_context.window = CGL_window_create(512, 512, "CGL Easy"); \
    if(g_context.window == NULL) return false; \
    CGL_window_make_context_current(g_context.window);\
    if(!CGL_gl_init()) return EXIT_FAILURE; \
    g_context.framebuffer = CGL_framebuffer_create_from_default(g_context.window);\
    if(g_context.framebuffer == NULL) return false; \
    if(!CGL_widgets_init()) return EXIT_FAILURE;  \
    CGL_noise_init();\
    g_context.is_running = true; g_context.is_drawing = true; g_context.is_updating = true; \
    CGL_float curr_time = CGL_utils_get_time(), prev_time = curr_time, delta_time = 0.0f; \
    setup(); \
    while(!CGL_window_should_close(g_context.window) && g_context.is_running)\
    {\
        curr_time = CGL_utils_get_time(); delta_time = curr_time - prev_time; prev_time = curr_time;\
        if(g_context.is_updating) update(delta_time); \
        CGL_framebuffer_bind(g_context.framebuffer); \
        CGL_gl_clear(0.0f, 0.0f, 0.0f, 1.0f); \
        if(g_context.is_drawing) draw(delta_time);\
        CGL_window_swap_buffers(g_context.window);\
        CGL_window_poll_events(g_context.window); \
    }\
    cleanup();\
    CGL_noise_shutdown();\
    CGL_widgets_shutdown();\
    CGL_framebuffer_destroy(g_context.framebuffer);\
    CGL_gl_shutdown(); \
    CGL_window_destroy(g_context.window);\
    CGL_shutdown(); \
    return EXIT_SUCCESS; \
}